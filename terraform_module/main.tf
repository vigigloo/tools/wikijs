resource "kubernetes_secret" "postgresqlPassword" {
  metadata {
    name      = "postgresql-password"
    namespace = var.namespace
  }
  data = {
    postgresqlPassword = var.wikijs_postgresql_postgresqlPassword
  }
}

resource "helm_release" "wikijs" {
  chart           = "wiki"
  repository      = "https://charts.js.wiki"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/wikijs.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.wikijs_postgresql_postgresqlPassword == null ? [] : [var.wikijs_postgresql_postgresqlPassword]
    content {
      name  = "postgresql.existingSecretKey"
      value = keys(kubernetes_secret.postgresqlPassword.data)[0]
    }
  }

  dynamic "set" {
    for_each = var.wikijs_postgresql_postgresqlPassword == null ? [] : [var.wikijs_postgresql_postgresqlPassword]
    content {
      name  = "postgresql.existingSecret"
      value = kubernetes_secret.postgresqlPassword.metadata[0].name
    }
  }

  dynamic "set" {
    for_each = var.wikijs_postgresql_ssl == null ? [] : [var.wikijs_postgresql_ssl]
    content {
      name  = "postgresql.ssl"
      value = var.wikijs_postgresql_ssl
    }
  }

  dynamic "set_sensitive" {
    for_each = var.wikijs_postgresql_postgresqlUser == null ? [] : [var.wikijs_postgresql_postgresqlUser]
    content {
      name  = "postgresql.postgresqlUser"
      value = var.wikijs_postgresql_postgresqlUser
    }
  }

  dynamic "set" {
    for_each = var.wikijs_postgresql_postgresqlDatabase == null ? [] : [var.wikijs_postgresql_postgresqlDatabase]
    content {
      name  = "postgresql.postgresqlDatabase"
      value = var.wikijs_postgresql_postgresqlDatabase
    }
  }

  dynamic "set" {
    for_each = var.wikijs_postgresql_postgresqlPort == null ? [] : [var.wikijs_postgresql_postgresqlPort]
    content {
      name  = "postgresql.postgresqlPort"
      value = var.wikijs_postgresql_postgresqlPort
    }
  }

  dynamic "set" {
    for_each = var.wikijs_postgresql_host == null ? [] : [var.wikijs_postgresql_host]
    content {
      name  = "postgresql.postgresqlHost"
      value = var.wikijs_postgresql_host
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }
}
