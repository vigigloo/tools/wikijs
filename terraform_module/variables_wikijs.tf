variable "wikijs_postgresql_ssl" {
  type    = string
  default = null
}
variable "wikijs_postgresql_host" {
  type = string
}
variable "wikijs_postgresql_postgresqlPort" {
  type    = number
  default = null
}
variable "wikijs_postgresql_postgresqlDatabase" {
  type = string
}
variable "wikijs_postgresql_postgresqlUser" {
  type = string
}
variable "wikijs_postgresql_postgresqlPassword" {
  type = string
}